<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php if (isset($title)) : ?>
            <?php printf($title); ?>
        <?php else : ?>
        <?php printf("Mon site");
            endif; ?>
    </title>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <link href="https://fonts.googleapis.com/css?family=Permanent+Marker&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="./css/normalize.css">
    <link rel="stylesheet" href="./css/style.css">


</head>

<body>
    <!-- initialise ligne grid bootstrap -->
    <div class="container col-12 navbar-dark bg-dark ">
        <div class="row">

            <nav class="navbar navbar-expand-sm mr-3">
                <a href="index.php"><img src="img/logo.png" class="logoS" alt="Logo"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav mr-auto pl-3">
                        <li class="nav-item active">
                            <a class="nav-link" href="index.php">Accueil <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="formules.php">Formules</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="activities.php">Activité</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="dropdown10" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Plus</a>
                            <div class="dropdown-menu" aria-labelledby="dropdown10">
                                <a class="dropdown-item" href="blog.php">Blog</a>
                                <a class="dropdown-item" href="contact.php">Contact</a>
                                <a class="dropdown-item" href="apropos.php">A propos</a>
                                <a class="dropdown-item" href="mentions.php">Mentions légales</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
            <h1 class="col-6 mx-auto titL">Planet Workout</h1>
        </div>
    </div>