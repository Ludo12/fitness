<?php
$title = 'Nos Offres';
include 'elements/header.php'; ?>
<section class="services">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center services-title">
                <h1 class="text-uppercase text-title">Découvrez nos offres d'abonnement</h1>
            </div>
        </div>
        <div class="container services-list">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="services-bis">
                        <div class="services-img text-center py-4">
                            <img src="./img/basique.svg" alt="Basique">
                        </div>
                        <div class="card-body text-center">
                            <h5 class="card-title text-uppercase">basique</h5>
                            <p class="card-text text-secondary">Profitez d'un accès illimité !</p>
                            <ul>
                                <li>Accès illimité à la salle</li><br>
                                <li>1 séance par mois offerte d'une des activités proposées <a href="activities.php">ici</a></li>
                            </ul>

                            <h6>19.99€/mois</h6>
                            <button type="button" class="btn btn-danger">Souscrire</button>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="services-bis">
                        <div class="services-img text-center py-4">
                            <img src="./img/confort.svg" alt="Ruby">
                        </div>
                        <div class="card-body text-center">
                            <h5 class="card-title text-uppercase">Confort</h5>
                            <p class="card-text text-secondary">Profitez de l'expertise de nos coachs !</p>
                            <ul>
                                <li>Accès illimité à la salle</li><br>
                                <li>1 consultation avec un coach pour déterminer votre programme sportif</li><br>
                                <li>2 séances par mois offertes d'une des activités proposées <a href="activities.php">ici</a></li>
                            </ul>

                            <h6>29.99€/mois</h6>
                            <button class="btn btn-danger">Souscrire</button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="services-bis" id="premium">
                        <div class="services-img text-center py-4">
                            <img src="./img/premium.svg" alt="PHP">
                        </div>
                        <div class="card-body text-center">
                            <h5 class="card-title text-uppercase">Premium</h5>
                            <p class="card-text text-secondary"> Profitez à fond de nos services !</p>
                            <ul>
                                <li>accès illimité à la salle</li><br>
                                <li>Suivi personnel par un de nos coachs</li><br>
                                <li>partagez votre compte avec une autre personne de votre domicile</li><br>
                                <li>Invitez une autre personne avec vous lorsque vous venez vous entrainer</li><br>
                                <li>4 séances par mois offertes d'une des activités proposées <a href="activities.php">ici</a></li>
                            </ul>

                            <h6>49.99€/mois</h6>
                            <button class="btn btn-danger">Souscrire</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php include 'elements/footer.php'; ?>