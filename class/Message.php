<?php
class Message
{
    const LIMIT_USERNAME = 3;
    const LIMIT_MESSAGE = 3;
    //Propriete
    private $username;
    private $message;
    private $date;

    public static function fromJSON(string $json): Message
    {
        $data = json_decode($json, true);
        $date = $data['date'];
        return new self($data['username'], $data['message'], new DateTime("@$date"));
    }

    //Methode
    public function __construct(string $username, string $message, ?DateTime $date = null)
    {
        $this->username = $username;
        $this->message = $message;
        $this->date = $date ?: new DateTime();
    }
    public function isValid(): bool
    {
        return empty($this->getErrors());
    }

    public function getErrors()
    {
        $errors = [];
        if (strlen($this->username) < self::LIMIT_USERNAME) {
            $errors['username'] = 'Votre pseudo est trop court';
        }
        if (strlen($this->message) < self::LIMIT_MESSAGE) {
            $errors['message'] = 'Votre message est trop court';
        }
        return $errors;
    }

    public function toHTML(): string
    {
        $username = htmlentities($this->username);
        $message = nl2br(htmlentities($this->message));
        $this->date->setTimezone(new DateTimeZone('Europe/Paris'));
        return "<p><strong>{$username}</strong> <em>le {$this->date->format('Y-m-d')} à {$this->date->format('H:i')}</em><br>{$message}</p>";
    }

    public function toJSON()
    {
        return  json_encode(['username' => $this->username, 'message' => $this->message, 'date' => $this->date->getTimestamp()]);
    }
}
