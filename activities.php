<?php
$titre = 'Activités';
include 'elements/header.php';
?>

<!-- HEADER -->
<section class="activities">
    <div class="text-white pb-3 pt-3">
        <div id="debut">
            <h1 class="text-uppercase text-center">Activités</h1>
            <p class="lead pl-5">Chaque semaine, Planet Workout vous propose de nombreuses activités
                variés afin de compléter vos entraînements.
            </p>
        </div>
    </div>
    <div class="container col-12">

        <!-- AQUAPONEY -->
        <div class="row row-cols-2">
            <div class="col">
                <div id="aqua" class="d-flex m-4 border rounded shadow-sm align-items-stretch">
                    <div class="p-4 text-white">
                        <strong class="d-inline-block mb-2 text-primary">Sport aquatique</strong>
                        <h3><strong>Aquaponey</strong></h3>
                        <div class="mb-1 text-muted">Lundi</div>
                        <p class="card-text mb-auto">Rien de mieux que la piscine pour démarrer en douceur
                            sa reprise en main avec nos amis les poneys
                        </p>
                        <a href="#" class="stretched-link">En savoir plus</a>
                    </div>
                    <img class="rounded float-left" style="height : 220px;width :220px;object-fit: cover;" src="./img/aqua.jpg" alt="">

                </div>
            </div>
            <!-- RUNNING -->
            <div class="col">
                <div id="running" class="d-flex m-4 border rounded shadow-sm">
                    <div class="p-4 text-white">
                        <strong class="d-inline-block mb-2 text-success">En plein air</strong>
                        <h3><strong>Running</strong></h3>
                        <div class="mb-1 text-muted">Samedi</div>
                        <p class="mb-auto">Courir c'est important, The Walking Dead nous l'a montré, soyez prêts...
                        </p>
                        <a href="#" class="stretched-link">En savoir plus</a>
                    </div>
                    <img class="rounded float-left" style="height : 220px;width :220px;object-fit: cover;" src="./img/running_woman.png" alt="">

                </div>
            </div>


            <!-- YOGA -->
            <div class="col">

                <div id="yoga" class="d-flex m-4 border rounded shadow-sm">
                    <div class="p-4 text-white">
                        <strong class="d-inline-block mb-2 text-primary">Relaxation</strong>
                        <h3><strong>Yoga</strong></h3>
                        <div class="mb-1 text-muted">Mercredi</div>
                        <p class="card-text mb-auto">Activité réservée aux retraités et vegan nourris au sans-gluten (ou
                            toute espèce apparentée)
                        </p>
                        <a href="#" class="stretched-link">En savoir plus</a>
                    </div>
                    <img class="rounded float-left" style="height : 220px;width :220px;object-fit: cover;" src="./img/logo_2.png" alt="">

                </div>
            </div>
            <!-- BOXE -->
            <div class="col">
                <div id="boxe" class="d-flex m-4 border rounded shadow-sm">
                    <div class="p-4 text-white">
                        <strong class="d-inline-block mb-2 text-success">Sport de combat</strong>
                        <h3><strong>Boxe</strong></h3>
                        <div class="mb-1 text-muted">Vendredi</div>
                        <p class="mb-auto">Car avoir des muscles c'est bien mais savoir les utiliser c'est mieux !</p>
                        <a href="#" class="stretched-link">En savoir plus</a>
                    </div>
                    <img class="rounded float-left" style="height: 220px;width:220px;" src="./img/boxing.jpg" alt="">

                </div>
            </div>
        </div>
    </div>
</section>

<?php
include 'elements/footer.php';
?>