<?php

require_once 'class/Message.php';
require_once 'class/GuestBook.php';
$errors = null;
$success = false;
$guestbook = new GuestBook(dirname(__DIR__) . DIRECTORY_SEPARATOR . "fitness" . DIRECTORY_SEPARATOR . "message" . DIRECTORY_SEPARATOR . "message");

if (isset($_POST["username"], $_POST["message"])) {
    $message = new Message($_POST["username"], $_POST["message"]);
    if ($message->isValid()) {
        $guestbook->addMessage($message);
        $success = true;
        $_POST = [];
    } else {
        $errors = $message->getErrors();
    }
}

$messages = $guestbook->getMessages();
$title = "Blog";
require "elements/header.php";
?>


<div class="body_contact">
    <div class="container blog">
        <h1 class="text-uppercase text-center">Blog</h1>
        <?php if (!empty($errors)) : ?>
            <div class="alert alert-danger">
                Formulaire invalide
            </div>
        <?php endif ?>
        <?php if ($success) : ?>
            <div class="alert alert-success">
                Merci pour votre message
            </div>
        <?php endif ?>
        <form action="" method="post">
            <div class="form-group mb-3">
                <textarea name="username" placeholder="Votre pseudo" class="form-control <?php isset($errors['username']) ? printf('is-invalid') : '' ?>" value="<?php printf(htmlentities($_POST['username'])) ?? '' ?>"></textarea>
                <?php if (isset($errors['username'])) : ?>
                    <div class="invalid-feedback"><?php printf($errors['username']); ?></div>
                <?php endif ?>

            </div>
            <div class="form-group mb-3">
                <textarea name="message" placeholder="Votre message" class="form-control <?php isset($errors['message']) ? printf('is-invalid') : '' ?>" value="<?php printf(htmlentities($_POST['message'])) ?? '' ?>"></textarea>
                <?php if (isset($errors['message'])) : ?>
                    <div class="invalid-feedback"><?php printf($errors['message']); ?></div>
                <?php endif ?>

            </div>

            <button type="submit" class="btn btn-danger">Envoyer</button>
        </form>
        <?php if (!empty($messages)) : ?>
            <h1 class="mt-4">Vos messages :</h1>
            <?php foreach ($messages as $message) : ?>
                <?php printf($message->toHtml()); ?>
            <?php endforeach ?>
        <?php endif ?>
    </div>
</div>

<?php require 'elements/footer.php'; ?>