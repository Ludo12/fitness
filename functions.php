<?php

function select(string $name, $value, array $options): string
{
    $html_options = [];
    foreach ($options as $key => $option) {
        $attributes = $key == $value ? 'selected' : '';
        $html_options[] = "<option value='$key' $attributes>$option</option>";
    }
    return "<select class='form-control' name='$name'>" . implode($html_options) . "</select>";
}
function dump($variable)
{
    printf("<pre>");
    var_dump($variable);
    printf("</pre>");
}
function creneaux_html(array $creneaux)
{
    if (empty($creneaux)) {
        return "Fermé";
    }
    $phrases = [];
    foreach ($creneaux as $creneau) {
        $phrases[] = "de <strong>{$creneau[0]}h</strong> à <strong>{$creneau[1]}h</strong>";
    }
    return "Ouvert " . implode(" et ", $phrases);
}

function in_creneaux(int $heure, array $creneaux): bool
{
    foreach ($creneaux as $creneau) {
        $debut = $creneau[0];
        $fin = $creneau[1];
        if ($heure >= $debut && $heure < $fin) {
            return true;
        }
    }
    return false;
}
